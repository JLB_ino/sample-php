<?php
	$obj = new MyClass();
	MyClassWriter::modify_obj($obj,"花子",25);
	MyClassWriter::print_obj($obj);

	class MyClass {
		private $age = 0;
		private $name = "Anonymous";

		function getAge(){
			return $this->age;
		}
		function setAge($s){
			$this->age = abs($s * 1);
		}

		function getName(){
			return $this->name;
		}
		function setName($s){
			$this->name = $s;
		}

		function print_me(){
			echo "名前は" . $this->name . ", 年齢は" . $this->age;
		}
	}

	class MyClassWriter {

		static function modify_obj($obj,$s,$n){
			$obj->setName($s);
			$obj->setAge($n);
		}

		static function print_obj($obj){
			echo "名前は" . $obj->getName();
			echo "\n";
			echo "年齢は" . $obj->getAge();
		}

	}
?>