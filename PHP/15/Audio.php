﻿<?php
interface Audio {
	public function play();

	public function stop();
}

class Stereo implements Audio {
	public function play() {
		echo "ステレオを再生します。";
	}

	public function stop() {
		echo "ステレオを停止します。";
	}
}

class MP3Player implements Audio {
	public function play() {
		echo "MP3を再生します。";
	}

	public function stop() {
		echo "MP3を停止します。";
	}
}

$stereo = new Stereo();
$stereo->play();
$mp3player = new Mp3player();
$mp3player->play()
?>