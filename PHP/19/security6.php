﻿<?php 
// セッションを開始 
session_start(); 

//セッションハイジャック対策 
session_regenerate_id(true); 

// DBXクラスの読み込み 
require_once "dbx.php"; 

//security6.htmlから受け取った値を変数に登録 
$login = $_POST["login"]; 
$password = $_POST["password"]; 

// ログイン名・パスワードが入力されていることを確認 
if (empty($login) || empty($password)) { 
	unset($_SESSION['login']); unset($_SESSION['name']); 
	exit("ログイン名・パスワードが入力されていません"); 
}
try { 
	// データベースに接続する 
	DBX::connect(); 
	// SQL文を発行して、ログイン・パスワードが適切かどうか確認 
	$sql = "SELECT name FROM member 
		 WHERE login = :login AND password = :password;";
	// SQL文の実行準備 
	$result = DBX::$pdo->prepare($sql);
	// プレースホルダに値をバインド(クォートとエスケープ) 
	$result->bindValue(":login", $login); 
	$result->bindValue(":password", $password); 
	// SQL文の実行 
	$result->execute(); 
	if ($result->rowCount() == 0) { 
		unset($_SESSION['login']); 
		unset($_SESSION['name']); 
		exit( "ログイン・パスワードが適切ではありません。"); 
	} else { 
		print "ログインに成功しました。<br>"; 
		$name = $result->fetchColumn(); 
		$_SESSION['login'] = $login;
		$_SESSION['name'] = $name;    
	}
} catch (PDOException $e) {    
	exit($e->getMessage()); 
} 
?> 
<a href="login2.php">ログイン状態を確認する</a><!--「セッション」章のプログラム-->