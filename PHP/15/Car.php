﻿<?php
class Car {
	public function accel() {
		echo "ガソリンで動く";
	}
}

class ElectricCar extends Car {
	public function accel() {
		echo "電気で動く";
	}
}

$obj = new ElectricCar;
$obj->accel();

?>