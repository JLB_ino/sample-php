<?php
class DBConnector {
	private static $db;
	public static function getDB() {
		if (is_null ( self::$db )) {
			$userName = 'root';
			$password = 'himitu';
			$host = 'localhost';
			$dbName = 'phpdata';

			try {
				$dsn = 'mysql:host=' . $host . ';dbname=' . $dbName . ';charset=utf8';
				self::$db = new PDO ( $dsn, $userName, $password );
			} catch ( PDOException $e ) {
				print $e->getMessage ();
			}
		}
		return self::$db;
	}
	public static function disconnectDB() {
		if (! is_null ( self::$db ))
			self::$db = null;
	}
}