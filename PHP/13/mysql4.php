﻿<?php 
$link = mysqli_connect('localhost', 'root', 'himitu', 'sample'); 
//データベース接続状況の確認
if (mysqli_connect_errno($link)){
        echo "データベース接続に失敗しました: " . mysqli_connect_error();
}
// 実行したいSQL文の定義
$sql = "SELECT * FROM staff";
// SQL文の発行
$result = mysqli_query($link, $sql);
if (!$result) {
        echo "エラー：" . mysqli_error($link); 
        exit("<br>SQL文の発行に失敗しました。");
} else {
        echo "SQL文の発行に成功しました。";
}
// 実行結果の行数を取得する
$rows = mysqli_num_rows($result); 
if ($rows < 1) { 
        exit("データが存在しませんでした。"); 
} else {
        echo "{$rows} 件のデータが存在しました。"; 
}
?>