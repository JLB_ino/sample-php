<html>
	<head>
		<meta http-equiv="content-type"
			content="text/html; charset=utf-8">
		<title>PHP Sample</title>
	</head>
	<body>
<?php
	echo "getdate使用</br>";
	$dt = getdate();
	$y = $dt['year'];
	$m = $dt['mon'];
	$d = $dt['mday'];
	$h = $dt['hours'];
	$mm = $dt['minutes'];
	$sc = $dt['seconds'];
	echo "$y/$m/$d $h:$mm:$sc";
?>
<hr/>
<?php
	echo "localtime使用</br>";
	$dt = localtime(time(),TRUE);
	$y = $dt['tm_year'] + 1900;
	$m = $dt['tm_mon'] + 1;
	$d = $dt['tm_mday'];
	$h = $dt['tm_hour'];
	$mm = $dt['tm_min'];
	$sc = $dt['tm_sec'];
	echo "$y/$m/$d $h:$mm:$sc";
?>

	</body>
</html>
