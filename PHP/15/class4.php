﻿<?php
class MyClass {
	private $name;    
	private $age;    
	public function __construct($name, $age) {        
		$this->__set("name", $name);        
		$this->__set("age", $age); 
	}    
	public function __set($var, $value)    { 
		switch ($var) { 
			// nameプロパティ値のチェック 
			case "name": 
				if (strlen($value) > 50) { 
					exit("名前エラー:長すぎる名前"); 
				} 
				break; 
			// ageプロパティ値のチェック 
			case "age": 
				if (!is_numeric($value)) { 
					exit("年齢エラー:数値ではない");
				} elseif ($value < 0) { 
					exit("年齢エラー:負の値");
				}                
				break;
			default: 
				exit("存在しないプロパティ"); 
		}
		// プロパティに値を代入 
		$this->{$var} = $value; 
	}    
	public function __get($var)    { 
		// プロパティの値を返す        
		return $this->{$var}; 
	} 
}

$obj = new MyClass("山田", -5); 
echo "名前:" . $obj->name . "<br>"; 
echo "年齢:" . $obj->age . "<br>";
?>