<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title>PHP Sample</title>
	<style>
	h3 {
		font-weight: bold;
		border-style: none none solid solid;
		border-width: 0px 0px 3px 7px;
		border-color: #0000ff;
		padding: 5px 0px 0px 0px;
		color: #000066;
		background-color: #ddddff;
		width: 100%;
	}
	</style>
</head>
<body>
<h3>MySQL Sample</h3>
<?php
	echo "<table border=1>";
	echo "<tr><th>ID</th><th>TITLE</th><th>ABSTRACT</th><th>CONTENT</th><th>DATE</th></tr>";
	try {
		$pdo = new PDO("mysql:host=localhost; dbname=sample","root", "himitu");
		$stmt = $pdo->query("SELECT * FROM myblog");

		while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
			echo "<tr>";
			foreach ($row as $val) {
				echo "<td>" . $val . "</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	} catch (PDOException $e){
		echo "ERROR: " . $e->getMessage();
	}

	$pdo = null;
?>

</body>
</html>
