﻿<?php
class MyClass { 
        public $num1; 
        public $num2; 
        public function plus() {
                return $this->num1 + $this->num2; 
        }
        public function minus() { 
                return $this->num1 - $this->num2; 
        }
}
// オブジェクトを生成 
$obj1 = new MyClass;
$obj1->num1 = 100;
$obj1->num2 = 50; 
$obj2 = new MyClass; 
$obj2->num1 = 60; 
$obj2->num2 = 30; 
?>
<html><body> 
1つめのオブジェクト <br> 
plusメソッドの結果 : <?php echo $obj1->plus(); ?><br> 
minusメソッドの結果 : <?php echo $obj1->minus(); ?><br> 
2つめのオブジェクト <br> 
plusメソッドの結果 : <?php echo $obj2->plus(); ?><br> 
minusメソッドの結果 : <?php echo $obj2->minus(); ?><br> 
</body></html>