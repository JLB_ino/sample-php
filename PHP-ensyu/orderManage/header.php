<SCRIPT type=text/javascript>
    function displayDate(){
      var this_month = new Array(12);
      this_month[0]  = "January";
      this_month[1]  = "February";
      this_month[2]  = "March";
      this_month[3]  = "April";
      this_month[4]  = "May";
      this_month[5]  = "June";
      this_month[6]  = "July";
      this_month[7]  = "August";
      this_month[8]  = "September";
      this_month[9]  = "October";
      this_month[10] = "November";
      this_month[11] = "December";
      var today = new Date();
      var day   = today.getDate();
      var month = today.getMonth();
      var year  = today.getYear();
      if (year < 1900){
         year += 1900;
      }
      return(day+" "+this_month[month]+" " +year);
    }
</SCRIPT>

<table summary="layout" style="border-collapse:collapse;" >
  <tr>
 	<td bgcolor="#FFFFFF" width="10"></td>
    <td bgcolor="#FFFFFF" align="left">
      <FONT color="#7A7A7A">
      <SCRIPT type=text/javascript>
        document.write(displayDate());
      </SCRIPT>
      </FONT>
    </td>
  </tr>
</table>

<TABLE summary="layout" style="border-collapse:collapse;">
    <tr>
      <TD align="left" bgcolor="#2E2E6A">
        <BR>
        <!-- タイトル編集 -->
	<font color = "white" size="6"><b><i>ProductOrder Sample</i></b></font>
		<!-- 編集終了 -->
        <BR>
        <BR>
      </TD>
    </tr>
    <tr>
      <TD align="right" height="30" bgcolor="#696969">
          <FONT size="+1"><a href="topController.php">トップ画面へ</a></FONT>
      </TD>
    </tr>
</TABLE>

<BR>
<style>table {width: 100%; table-layout: fixed;}</style>
