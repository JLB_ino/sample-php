﻿<?php
	$a = 10;
	switch( $a ){
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			echo '$a は0 ～ 4 の値です。';
			break;
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			echo '$a は5 ～ 9 の値です。';
			break;
		default:
			echo '$a は0 ～ 9 のいずれでもありません。';
	}
?>