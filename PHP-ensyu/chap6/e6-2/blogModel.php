<?php
require_once 'DBConnector.php';

/*
 * モデルはコントローラーの指示に従ってDBアクセスを行う
 */
class blogModel {
	protected $db;
	public function __construct() {
		$this->db = DBConnector::getDB ();
	}
	public function searchBlogAll() {
		try {
			$sth = $this->db->prepare ( "select * from myblog" );
			$sth->execute ();
			$sth->setFetchMode ( PDO::FETCH_ASSOC );
			$arr = array ();
			while ( $row = $sth->fetch () ) {
				$arr [] = $row;
			}
			DBConnector::disconnectDB ();
			return $arr;
		} catch ( PDOException $e ) {
			echo "ERROR: " . $e->getMessage ();
		}
	}
	public function searchBlogId($blogId) {
		// $query = "select count(*) from myblog where id=" . $blogId;
		$query = "select * from myblog where id=" . $blogId;
		try {
			$sth = $this->db->prepare ( $query );
			$sth->execute ();
			$sth->setFetchMode ( PDO::FETCH_ASSOC );
			$arr = array ();
			while ( $row = $sth->fetch () ) {
				$arr [] = $row;
			}
			DBConnector::disconnectDB ();
			return $arr;
		} catch ( PDOException $e ) {
			echo "ERROR: " . $e->getMessage ();
		}
	}
	public function addBlog($blogTitle, $blogAbstract, $blogContent) {
		$query = 'insert into myblog (title,abstract,content,uptime)
					values(:inTitle,:inAbstract,:inContent,CURDATE())';
		try {
			$sth = $this->db->prepare ( $query );
			$sth->bindParam ( ":inTitle", $blogTitle );
			$sth->bindParam ( ":inAbstract", $blogAbstract );
			$sth->bindParam ( ":inContent", $blogContent );
			$sth->execute ();
			DBConnector::disconnectDB ();
		} catch ( PDOException $e ) {
			echo "ERROR: " . $e->getMessage ();
		}
	}
	public function deleteBlog($blogId) {
		try {
			$query = "delete from myblog where id=" . $blogId;
			$sth = $this->db->prepare ( $query );
			$sth->execute ();
			DBConnector::disconnectDB ();
		} catch ( PDOException $e ) {
			echo "ERROR: " . $e->getMessage ();
		}
	}
}