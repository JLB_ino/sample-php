<!-- Viewはbodyの内容を記述するのみ -->
<!-- ユーザーに表示させたいデータがdataインスタンスに格納されている。 -->
<!-- Viewはdataインスタンスを利用してどのようにユーザーに見せるかという -->
<!-- 描画部分を担当する -->
<?php
foreach ( $this->data as $value ) {
	echo "<h2>" . $value ['title']. "<font size=-1>[" . $value ['id'] . "]</font></h2>";
	echo "<h3>" . $value ['abstract']. "</h3>";
	echo "<p>" . preg_replace ( "/\r\n/", "<br/>", $value ['content'] ). "</p>";
	echo "<h4>" . date ( 'Y年 m月 d日', strtotime ( $value ['uptime'] ) ). "</h4>";
}
?>
