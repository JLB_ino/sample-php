<?php 
// DBXクラス定義を読み込み（同フォルダ内にdbx.phpを設置）
require_once "dbx.php"; 
try { 
	// データベースに接続する 
	DBX::connect(); 
	// SQL文の作成 
	$sql = "SELECT * FROM staff";
	// SQL文の実行 $
	$state = DBX::$pdo->query($sql); 
	// 結果の取得 
	$list = $state->fetchAll(PDO::FETCH_ASSOC); 
} catch (PDOException $e) { 
	// 例外処理    
	exit($e->getMessage());
}
?>

<html> <body> 
<table border="1"> 
	<tr> 
		<th>社員番号</th> 
		<th>氏名</th> <th>電話番号</th>    
	</tr> 
<?php foreach ($list as $data) { ?>    
	<tr>        
		<td><?php echo $data["code"]; ?></td>        
		<td><?php echo $data["name"]; ?></td>        
		<td><?php echo $data["phone"]; ?></td>    
	</tr> 
<?php } ?> 
</table> 
</body> </html>