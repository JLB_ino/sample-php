<?php
function showAll($lang, $data) {
	if (! function_exists ( 'showUpper' )) {
		function showUpper($data) {
			$n = 0;
			foreach ( $data as $val ) {
				echo $n ++ . ':' . strToUpper ( $val ) . "\n";
			}
		}
	}

	if (! function_exists ( 'showLower' )) {
		function showLower($data) {
			$n = 0;
			foreach ( $data as $val ) {
				echo $n ++ . ':' . strToLower ( $val ) . "\n";
			}
		}
	}

	if (is_array ( $data ) == FALSE) {
		return FALSE;
	}
	if ($lang == 'U') {
		showUpper ( $data );
	} elseif ($lang == 'L') {
		showLower ( $data );
	} else {
		return FALSE;
	}
	return TRUE;
}

$arr1 = array (
		'Welcome',
		'Real',
		'World'
);

$flg1 = showAll ( 'U', $arr1 );
$flg2 = showAll ( 'L', $arr1 );

if ($flg1 == FALSE) {
	echo 'CANNOT SHOW!';
}
if ($flg2 == FALSE) {
	echo 'CANNOT SHOW!';
}

?>