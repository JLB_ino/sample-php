<?php
	$obj = new MyClass2("太郎",20);
	$obj->print_me();

	class MyClass {
		private $age = 0;
		private $name = "Anonymous";

		public function __construct($s,$n){
			$this->setName($s);
			$this->setAge($n);
		}

		function getAge(){
			return $this->age;
		}
		function setAge($s){
			$this->age = abs($s * 1);
		}

		function getName(){
			return $this->name;
		}
		function setName($s){
			$this->name = $s;
		}

		function print_me(){
			echo "名前は" . $this->name . ", 年齢は" . $this->age;
		}
	}

	class MyClass2 extends MyClass {
		function print_me(){
			echo "名前は" . $this->getName();
			echo "\n";
			echo "年齢は" . $this->getAge();
		}
	}
?>