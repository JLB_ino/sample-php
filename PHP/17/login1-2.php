﻿<?php
// セッションを開始 
session_start();
// DBXクラスの読み込み 
require_once "dbx.php"; 

// login1.htmlから受け取った値を変数に登録 
$login = $_POST["login"]; 
$password = $_POST["password"]; 

// ログイン名・パスワードが入力されていることを確認
 if (strlen($login) == 0 || strlen($password) == 0) {
	unset($_SESSION['login']); 
	unset($_SESSION['name']); 
	exit("ログイン名・パスワードが入力されていません"); 
} 
try {
	// データベースに接続する 
	DBX::connect(); 
	// SQL文を発行して、ログイン・パスワードが適切かどうか確認 
	$sql = "SELECT name FROM member
			WHERE login = '{$login}' AND password = '{$password}'";   
	$state = DBX::$pdo->query($sql); 
	if ($state->rowCount() > 0) { 
		echo "ログインに成功しました。<br>";
		$name = $state->fetchColumn(); 
		$_SESSION['login'] = $login;
		$_SESSION['name'] = $name; 
	} else { 
		unset($_SESSION['login']); 
		unset($_SESSION['name']); 
		exit("ログイン名・パスワードが適切ではありません。");    
	}
} catch (Exception $e) { 
	exit($e->getMessage()); 
}