<?php
	include "recording.php";
	$records = array(
		new Recording("ローマの祝日",false, "アンネ王女のローマでの滞在を描いた作品"),
		new Recording("パリの恋人たち",true, "パリの女優とカメラマン恋のものがたり"),
		new Recording("おしゃれな泥棒",false, "美術館に泥棒に入るおしゃれな二人を描いたラブコメディ"),
		new Recording("マイ・レディ",true, "ラブストーリーミュージカルの傑作")
	);
	echo "--------------作　品　紹　介-------------" . "\n";
	for($i = 0; $i < count($records); $i++){
		echo "タイトル　：" . $records[$i]->title . "\n";
		if($records[$i]->newOrOld){
			echo "　(新作登場!!)" . "\n";
		}
		echo "新旧区分　：" . ($records[$i]->newOrOld ? '新作' : '旧作') . "\n";
		echo "ストーリー：" . $records[$i]->contents . "\n";
		echo "-----------------------------------------" . "\n";
	}
?>