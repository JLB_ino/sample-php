<?php

	include "customer.php";

    // 口座開設時に顧客名　田中　太郎、　入金額　 50000円 で 顧客 オブジェクトの生成
    echo "ある顧客が口座を開設します" . "\n";
    $customer = new Customer("田中　太郎",new Account(50000));

    // 残高の表示
    echo "顧客 [" . $customer->name . "] さんの残高は " . $customer->account->balance . " 円です" . "\n";

    // 15000円 引き出し
    echo "顧客 [" . $customer->name . "] さんが15000円 引き出します" . "\n";
    $customer->account->withdraw(15000);

    // 12000円 入金
    echo "顧客 [" . $customer->name . "] さんが12000円入金します" . "\n";
    $customer->account->deposit(12000);

    // 32000円 引き出し
    echo "顧客 [" . $customer->name . "] さんが32000円 引き出します" . "\n";
    $customer->account->withdraw(12000);

    // 残高の表示
    echo "顧客 [" . $customer->name . "] さんの残高は " . $customer->account->balance . " 円です" . "\n";

?>