<?php
/*
 * コントローラーの各メソッドではユーザーの要求を満たすために 　まずはモデルを利用して、データを集め、集めたデータをビューに 　渡して表示させる。つまり、コントローラーはモデルとビューの 　橋渡しの役目を担う。
 */
require_once 'encode.php';
require_once 'view.php';
require_once 'stockModel.php';

// リクエスト内容により処理の振り分けを行う
if (isset ( $_REQUEST ['request'] )) {
	$_func = e ( $_REQUEST ['request'] );
	/* 可変関数を用いることで一行で処理の振り分けを行うことでできる */
	$_func ();
} else {
	showList ();
}

/* 一覧表示 */
function showList() {
	$model = new stockModel ();
	$result = $model->select ();
	$view = new View ( $result );
	$view->render ( "stock_list.php" );
}

?>
