﻿<?php
  $to = "sample@dummydomain.com";
  $from = "php-basic@linuxacademy.ne.jp";
  $subject = "Test php sending.";
  $body = "こんにちは、このメールは無事に見ることができましたか？";
  $headers = "From: " . $from . "\r\n";
  $options = "-f" . $from;
  mb_language("ja");
  mb_internal_encoding("UTF-8");
  if (mb_send_mail($to, $subject, $body, $headers, $options)) {
    echo " メールの送信に成功しました。";
  } else {
    echo " メールの送信に失敗しました。";
  }
?>