<?php
	function tu_getYoubi($t){
		$youbiArray = array('Sun' => '日','Mon' => '月','Tue' => '火',
			'Wed' => '水','Thu' => '木','Fri' => '金','Sat' => '土');
		$youbi = date('D',$t);
		return $youbiArray[$youbi];
	}

	function tu_getFormattedDate($t){
		$fmt = "Y年n月j日";
		$dstr = date($fmt,$t) . '（' . tu_getYoubi($t) . '）';
		return  $dstr;
	}
?>
