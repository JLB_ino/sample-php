﻿<?php 
// 例外サブクラスの定義 
class SubException extends Exception {
}

// try?～catch文 
try { 
	if (empty($_GET['id'])) { 
		// 標準の例外クラスオブジェクトを投げる 
		throw new Exception("入力がありません"); 
	} elseif(!is_numeric($_GET['id'])) { 
		// 例外サブクラスのオブジェクトを投げる 
		throw new SubException("数値ではありません"); 
	} 
	// 入力値を出力する 
	echo "ID:{$_GET['id']}"; 
} catch (Exception $e) { 
	// 標準例外クラスのオブジェクトをキャッチ 
	exit("Exceptionクラスによる例外処理:" . $e->getMessage()); 
} catch (SubException $e) { 
	// 例外サブクラスのオブジェクトをキャッチ 
	exit("SubExceptionクラスによる例外処理:" . $e->getMessage()); 
}