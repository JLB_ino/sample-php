﻿<?php
	// 税率の定義
	define( "ZEI", 1.05 );
	// 税込み金額を算出するユーザー定義関数
	function zeikomi($zeinuki, $zeiritu = ZEI){
		$kingaku = $zeinuki * $zeiritu;
		return $kingaku;
	}
?>
<html><body>
 消費税込み：<?php echo zeikomi(1000); ?>
<br>
 特別税(10%) 込み：<?php echo zeikomi(1000,1.10); ?>
<br>
</body></html>