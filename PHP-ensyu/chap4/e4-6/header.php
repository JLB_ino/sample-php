<script type=text/javascript>

    function displayDate(){

        var this_month = new Array(12);
        this_month[0]  = "January";
        this_month[1]  = "February";
        this_month[2]  = "March";
        this_month[3]  = "April";
        this_month[4]  = "May";
        this_month[5]  = "June";
        this_month[6]  = "July";
        this_month[7]  = "August";
        this_month[8]  = "September";
        this_month[9]  = "October";
        this_month[10] = "November";
        this_month[11] = "December";

        var today = new Date();
        var day   = today.getDate();
        var month = today.getMonth();
        var year  = today.getYear();

        if (year < 1900){
            year += 1900;
        }

        return(day+" "+this_month[month]+" " +year);
    }
</script>

<?php
    function colorizeText($str, $c){
    	
    	echo '<b><font color=' . $c . '>' . $str . '</font></b>';
	}
?>

<table summary="layout" style="border-collapse:collapse;" >
    <tr>
        <td bgcolor="#FFFFFF" width="10"></td>
        <td bgcolor="#FFFFFF" align="left">
            <font color="#7A7A7A">
                <script type=text/javascript>
                    document.write(displayDate());
                </script>
            </font>
        </td>
    </tr>
</table>

<table summary="layout" style="border-collapse:collapse;" >
    <tr>
        <td align="left" bgcolor="#2E2E6A"><br/>
            <!-- タイトル編集 -->
	        <font color = "white" size="6"><b><i>sample_header</i></b></font><br/><br/>
		    <!-- 編集終了 -->
        </td>
    </tr>
    <tr>
        <td align="right" height="30" bgcolor="#696969">
            <font size="-1">　</font>
        </td>
    </tr>
</table>
<br/>