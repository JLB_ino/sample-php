drop table myblog;

create table myblog(
id int not null auto_increment primary key,
title varchar(255),
abstract varchar(255),
content text,
uptime date)
engine MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into myblog(id,title,abstract,content,uptime)
values
(1,"PHP Symphony","PHP Symphony is Light","PHP Symphony Blog Contents",CURDATE());
insert into myblog (title,abstract,content,uptime)
values
("RubyOnRails","RubyOnRails is Good","RubyOnRails blog Contents",CURDATE());

select * from myblog;