$(function() {
    // すべてのdiv要素を取得
    var $div = $('div');
    console.log($div); //=> <div id="div1">...</div>, <div id="div2">...</div>

    // id名がdiv1の要素を取得
    var $div1 = $('#div1');
    console.log($div1); //=> <div id="div1">...</div>

    // id名がdiv1の子要素のspan要素を取得
    var $span = $('#div1 span');
    console.log($span); //=> <span id="span1"></span>, <span id="span2"></span>
});
