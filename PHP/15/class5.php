﻿<?php 
class MyClass {    
	public $num1;    
	public $num2;    
	public function showProperties() {        
		echo $this->num1 . "<br>";        
		echo $this->num2 . "<br>";    
	} 
}

class MyClass2 extends MyClass {    
	public $num3;    
	public function showProperties() {        
		parent::showProperties();        
		echo $this->num3 . "<br>"; 
	}
} 
$obj1 = new MyClass; 
$obj1->num1 = 10; 
$obj1->num2 = 20; 
$obj2 = new MyClass2; 
$obj2->num1 = 100; 
$obj2->num2 = 200; 
$obj2->num3 = 300; 
?> 

<html><body> 
MyClassのオブジェクト<br> 
<?php $obj1->showProperties();?> 
MyClass2のオブジェクト<br> 
<?php $obj2->showProperties();?> 
</body></html>