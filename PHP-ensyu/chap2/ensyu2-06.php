<?php
	$obj = new MyClass();
	$obj->setName("太郎");
	$obj->setAge(20);
	$obj->print_me();

	class MyClass {
		private $age = 0;
		private $name = "Anonymous";

		function getAge(){
			return $this->age;
		}
		function setAge($s){
			$this->age = abs($s * 1);
		}

		function getName(){
			return $this->name;
		}
		function setName($s){
			$this->name = $s;
		}

		function print_me(){
			echo "名前は" . $this->name . ", 年齢は" . $this->age;
		}
	}
?>