<?php
	class Recording {
		private $title; //タイトル
		private $newOrOld; //新旧
		private $contents;//内容

		//コンストラクタ
		public function __construct($title, $newOrOld, $contents){
			$this->__set("title", $title);
			$this->__set("newOrOld", $newOrOld);
			$this->__set("contents", $contents);
		}

		public function __set($var, $value){
			$this->{$var} = $value;
		}

		public function __get($var){
			return $this->{$var};
		}
	}
?>