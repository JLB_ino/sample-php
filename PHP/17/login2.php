﻿<?php
// セッションを開始 
session_start(); 
// ログインされているかどうかを $_SESSION['login'] により確認
if (!isset($_SESSION['login']) || $_SESSION['login'] == "" ) {
	echo " ログインされていません。"; 
exit();
} else { 
	echo " 正常にログインできています。<br>"; 
	echo " あなたの名前は " . $_SESSION["name"] . " です。<br>";
}
echo " あなたのセッション ID は " . session_id() . " です。"; 
?>