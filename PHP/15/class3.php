﻿<?php 
class Person { 
 	public $id;    
	public $name;    
	public static $count = 0;    
	public static function getList($list) {        
		foreach ($list as $key => $value) {          
			$obj       = new Person;          
			$obj->id   = $key;            
			$obj->name = $value; 
			self::$count++;            
			$objlist[] = $obj; 
		}        
		return $objlist;    
	} 
}
$arr = array(1 => "田中", 2 => "佐藤", 3 => "鈴木"); 
$objects = Person::getList($arr); 
?>

<html><body> 
人数:<?php echo Person::$count; ?>人 
<table border="1">    
	<?php foreach ($objects as $obj) { ?>        
	<tr>            
		<td><?php echo $obj->id; ?></td>         
		<td><?php echo $obj->name; ?></td>       
	</tr>    
	<?php } ?> 
</table> 
</body></html>