create database phpdata;
use phpdata;

drop table gorder;
drop table gstock;

commit;

set names cp932;

create table gorder
(gid integer,
gname varchar(50),
gnumber integer)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into gorder values(1015,'FloppyDisc',2);
insert into gorder values(1001,'CPU',1);

create table gstock
(gid integer primary key,
gname varchar(50),
gnumber integer)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into gstock values(1001,'CPU',189);
insert into gstock values(1002,'Display',135);
insert into gstock values(1003,'HardDisk',160);
insert into gstock values(1004,'Memory',150);
insert into gstock values(1005,'Printer',250);
insert into gstock values(1006,'SoundBoard',190);
insert into gstock values(1007,'LAN Board',200);
insert into gstock values(1008,'VideoCard',160);
insert into gstock values(1009,'SCSI Board',185);
insert into gstock values(1010,'SCSI Cable',150);
insert into gstock values(1011,'Modem',95);
insert into gstock values(1012,'GraphicsBoard',135);
insert into gstock values(1013,'CD-ROM Drive',122);
insert into gstock values(1014,'MO Drive',115);
insert into gstock values(1015,'FloppyDisc',198);
commit;

select * from gorder;
select * from gstock;

grant all privileges on phpdata.* to root@"192.168.20.%"
identified by 'mysql' with grant option;