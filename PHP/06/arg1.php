﻿<?php
	function printArgs(){
		if( func_num_args()==0 ){
			echo " 引数はありません。<br>";
			exit();
		}
		foreach( func_get_args() as $param ){
			echo $param . "<br>";
		}
	}
	printArgs();
	echo "---<br>";
	printArgs( 10, 20, 30, "4 つめの引数", "5 つめの引数" );
?>