<?php 
class BMI {
	private $height;
	private $weight;
	public function setHeight($height){
		$this->height = $height;
	}
	public function getHeight(){
		return $this->height;
	}
	public function setWeight($weight){
		$this->weight = $weight;
	}
	public function getWeight(){
		return $this->weight;
	}
	public function getBmi(){
		$denominator = pow($this->height/100, 2);
		return $this->weight / $denominator;
	}
}
?>