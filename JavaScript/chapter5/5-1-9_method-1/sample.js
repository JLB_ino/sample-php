$(function() {
    // 要素を取得
    var $box = $('#box');

    // 要素のCSSを変更し、中身のHTMLを変更
    $box.css('color', 'red').html('<p>content</p>');
});
