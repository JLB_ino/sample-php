﻿<?php 
$link = mysqli_connect('localhost', 'root', 'himitu', 'sample'); 
//データベース接続状況の確認
if (mysqli_connect_errno($link)){
	echo "データベース接続に失敗しました: " . mysqli_connect_error();
}
// 実行したいSQL文の定義 
$sql = "SELECT * FROM staff";
// SQL文の発行 
$result = mysqli_query($link, $sql); 
if (!$result) { 
	echo "エラー：" . mysqli_error($link); 
	exit("<br>SQL文の発行に失敗しました。");
} else { 
	echo "SQL文の発行に成功しました。"; 
}
// 実行結果の行数を取得する 
$rows = mysqli_num_rows($result); 
if ($rows < 1) { 
	exit("データが存在しませんでした。"); 
}
// 実行結果データを取得する 
// 空の配列 $list を作成 
$list = array(); 
// while文で1行す?つ取得 
while ($data = mysqli_fetch_assoc($result)) {
	//$list に $data を追加    
	$list[] = $data; 
}
?> 
<html><body> 
<table border="1"> 
<tr> 
	<th>社員番号</th> 
	<th>氏名</th>
	<th>電話番号</th>
</tr>
<?php foreach ($list as $data) { ?>
<tr>        	
	<td><?php echo $data["code"]; ?></td>
	<td><?php echo $data["name"]; ?></td>
	<td><?php echo $data["phone"]; ?></td>
</tr> 
<?php } ?> 
</table> 
</body></html>