<?php
require_once 'DBConnector.php';

/*
 * モデルはコントローラーの指示に従ってDBからデータを取得する役名を負う
 */
class stockModel {
	protected $db;
	public function __construct() {
		$this->db = DBConnector::getDB ();
	}
	
	/* 一覧表示 */
	public function select() {
		try {
			$sth = $this->db->prepare ( "select * from gstock" );
			$sth->execute ();
			$sth->setFetchMode ( PDO::FETCH_ASSOC );
			$arr = array ();
			while ( $row = $sth->fetch () ) {
				$arr [] = $row;
			}
			return $arr;
		} catch ( PDOException $e ) {
			echo "ERROR: " . $e->getMessage ();
		}
	}
	
	/* 更新 */
	public function update($gid, $gnum) {
		$stmt = $this->db->prepare ( 'update gstock set GNUMBER=GNUMBER - :inGnum where GID =:inGid;
SQL' );
		$stmt->bindParam ( ":inGid", $gid );
		$stmt->bindParam ( ":inGnum", $gnum );
		$stmt->execute ();
	}
	
	/* 在庫チェック */
	public function checkStock($gid, $gnum) {
		try {
			$sth = $this->db->prepare ( "select gname from gstock where GID = :GID and GNUMBER >= :GNUMBER" );
			$sth->bindValue ( ':GID', $gid );
			$sth->bindValue ( ':GNUMBER', $gnum );
			$sth->execute ();
			$sth->setFetchMode ( PDO::FETCH_ASSOC );
			$gname = false;
			$row = $sth->fetch ();
			if ($row) {
				$gname = $row ['gname'];
			} else {
				echo "在庫数量がたりません";
				return $gname;
			}
			return $gname;
		} catch ( PDOException $e ) {
			echo "ERROR: " . $e->getMessage ();
		}
	}
}