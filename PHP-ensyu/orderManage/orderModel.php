<?php
require_once 'DBConnector.php';

/*
 * モデルはコントローラーの指示に従ってDBからデータを取得する役名を負う
 */
class orderModel {
	protected $db;
	public function __construct() {
		$this->db = DBConnector::getDB ();
	}
	public function select() {
		try {
			$sth = $this->db->prepare ( "select * from gorder" );
			$sth->execute ();
			$sth->setFetchMode ( PDO::FETCH_ASSOC );
			$arr = array ();
			while ( $row = $sth->fetch () ) {
				$arr [] = $row;
			}
			DBConnector::disconnectDB();
			return $arr;
		} catch ( PDOException $e ) {
			echo "ERROR: " . $e->getMessage ();
		}
	}
	public function insert($gid, $gname, $gnum) {
		$sth = $this->db->prepare ( 'insert into gorder (gid, gname, gnumber) values(:inGid,:inGname,:inGnum)' );
		$sth->bindParam ( ":inGid", $gid );
		$sth->bindParam ( ":inGname", $gname );
		$sth->bindParam ( ":inGnum", $gnum );
		$sth->execute ();
	}
}