<?php
/*
 * コントローラーの各メソッドではユーザーの要求を満たすために 　まずはモデルを利用して、データを集め、集めたデータをビューに 　渡して表示させる。つまり、コントローラーはモデルとビューの 　橋渡しの役目を担う。
 */
require_once 'encode.php';
require_once 'view.php';
require_once 'orderModel.php';
require_once 'stockModel.php';

//var_dump($_REQUEST ['request']) ;

// リクエスト内容により処理の振り分けを行う
if (isset ( $_REQUEST ['request'] )) {
	$_func = e ( $_REQUEST ['request'] );
	/* 可変関数を用いることで一行で処理の振り分けを行うことでできる */
	$_func ();
} else {
	showList ();
}

/* 一覧表示 */
function showList() {
	$model = new orderModel ();
	$result = $model->select ();
// 	var_dump($result);
	$view = new View ( $result );
	$view->render ( "order_list.php" );
}

/* 注文処理 */
function itemOrder() {
	// var_dump(isset($_POST['gid']));
	// var_dump(isset($_POST['qty']));
	if (isset ( $_POST ['gid'] ) && isset ( $_POST ['qty'] )) {
		$gid = e ( $_POST ['gid'] );
		$gnum = e ( $_POST ['qty'] );
		$stockModel = new stockModel ();
		$gname = $stockModel->checkStock ( $gid, $gnum );
		if ($gname) {
			$db = DBConnector::getDB ();
			try {
				$db->beginTransaction ();
				$orderModel = new orderModel ();
				$orderModel->insert ( $gid, $gname, $gnum );
				$stockModel->update ( $gid, $gnum );
				$db->commit ();
			} catch ( PDOException $e ) {
				echo "ERROR: " . $e->getMessage ();
				$db->rollBack ();
			}
		}
		showList();
		// var_dump($_REQUEST);
	} else {
		$view = new View ();
		$view->render ( "order_form.php" );
		// var_dump($_REQUEST);
	}
}

?>
