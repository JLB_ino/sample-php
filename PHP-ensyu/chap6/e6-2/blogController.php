<?php
/*
 * コントローラーの各メソッドではユーザーの要求を満たすために 　まずはモデルを利用して、データを集め、
 * 集めたデータをビューに 　渡して表示させる。つまり、コントローラーはモデルとビューの 　橋渡しの役目を担う。
 */
require_once 'encode.php';
require_once 'view.php';
require_once 'blogModel.php';

// var_dump($_REQUEST ['request']) ;

// リクエスト内容により処理の振り分けを行う
if (isset ( $_REQUEST ['request'] )) {
	$_func = e ( $_REQUEST ['request'] );
	/* 可変関数を用いることで一行で処理の振り分けを行うことでできる */
	$_func ();
} else {
	searchBlogAll ();
}

/* ブログ一覧表示 */
function searchBlogAll() {
	$model = new blogModel ();
	$result = $model->searchBlogAll ();
	// var_dump($result);
	$view = new View ( $result );
	$view->render ( "blog_list.php" );
}

/* ブログ検索 */
function searchBlogId() {
	if (isset ( $_POST ['blogId'] )) {
		$blogId = e ( $_POST ['blogId'] );
		$blogModel = new blogModel ();
		$result = $blogModel->searchBlogId ( $blogId );
		$view = new View ( $result );
		$view->render ( "blog_list.php" );
	} else {
		$view = new View ();
		$view->render ( "blog_search_form.php" );
	}
}

/* ブログ書き込み */
function addBlog() {
	if (isset ( $_POST ['blogTitle'] ) && isset ( $_POST ['blogAbstract'] )
		 && isset ( $_POST ['blogContent'] )) {
		$blogTitle = e ( $_POST ['blogTitle'] );
		$blogAbstract = e ( $_POST ['blogAbstract'] );
		$blogContent = e ( $_POST ['blogContent'] );
		$blogModel = new blogModel ();
		$blogModel->addBlog ( $blogTitle,$blogAbstract,$blogContent );
		searchBlogAll();
	} else {
		$view = new View ();
		$view->render ( "blog_add_form.php" );
	}
}

/* ブログ削除 */
function deleteBlog() {
	if (isset ( $_POST ['blogId'] )) {
		$blogId = e ( $_POST ['blogId'] );
		$blogModel = new blogModel ();
		$blogModel->deleteBlog( $blogId );
		searchBlogAll();
	} else {
		$view = new View ();
		$view->render ( "blog_delete_form.php" );
	}
}

?>
