$(function() {
    // 要素を取得
    var $box = $('#box');

    // 要素のCSSを変更
    $box.css('color', 'red');

    // 要素の中身のHTMLを変更
    $box.html('<p>content</p>');

    // 要素のclass名を取得
    var className = $box.attr('class');
    console.log(className); //=> content
});
