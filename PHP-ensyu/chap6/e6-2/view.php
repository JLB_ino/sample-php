<?php
class View {
	public $data; // ユーザーに見せたいデータを格納する変数
	public function __construct($data = NULL) {
		$this->data = $data;
	}
	public function setData($data) {
		$this->data = $data;
	}
	public function render($page) {
		print '<html>';
		print '<head>';
		print '<meta http-equiv="content-type"
			content="text/html; charset=utf-8">';
		print '<title>Blog Site</title>';
		print '<link href="sample_style.css" rel="stylesheet" type="text/css">';
		print '</head>';
		print '<body>';

		include ('header.php');

		// ボディ部分は要求されたファイル名を読み込むことで表示する
		require_once $page;

		print '</body>';
		print '</html>';
	}
}