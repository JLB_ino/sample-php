<?php
	include "account.php";

	class Customer {
		private $account; //口座
		private $name; //顧客名

		//コンストラクタ
		public function __construct($name,$account){
			$this->__set("name", $name);
			$this->__set("account", $account);
		}

		public function __set($var, $value){
			$this->{$var} = $value;
		}

		public function __get($var){
			return $this->{$var};
		}

	}
?>