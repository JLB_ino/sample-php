<?php
	include "account.php";

    // 初期金額として 50000円 を保持した Account オブジェクトの生成
    echo "50000円の残高の口座を作成します" . "\n";
	$account = new Account(50000);

	// 15000円 引き出し
	$account->withdraw(15000);
	echo "出金\t15000" . "\n";

    // 12000円 入金
    $account->deposit(12000);
    echo "入金\t12000" . "\n";

    // 32000円 引き出し
    $account->withdraw(32000);
	echo "出金\t32000" . "\n";

    // 残高の表示
    echo "---------------" . "\n";
	echo "残高\t" . $account->balance . "\n";
?>