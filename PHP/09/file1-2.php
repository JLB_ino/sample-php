﻿<html><body>
ファイルの内容
<hr>
<?php
  // ファイルオープン
  $fp = fopen("./data/test.txt", "r");
  // ファイルの終わりまで
  while (!feof($fp)) {
    // 1 行ずつ取得して表示
    echo fgets($fp);
    echo "<br>";
  }
  // クローズ
  fclose($fp)
?>
</body></html>