<?php
	$arr = array('one','two','three');
	echo '初期状態：';
	showArray($arr);
	echo "\n".'array_push後：';
	array_push($arr,'four');
	showArray($arr);
	echo "\n".'array_pop後：';
	array_pop($arr);
	showArray($arr);

	function showArray($ar){
		if (is_array($ar)){
			foreach($ar as $s){
				echo $s . " ";
			}
		}
		return null;
	}
?>