<html>
	<head>
		<meta http-equiv="content-type"
			content="text/html; charset=utf-8">
		<title>PHP Sample</title>
	</head>
	<body>

<?php
	$y = $_GET['year'];
	$m = $_GET['month'];
	$d = $_GET['day'];
	$n = $_GET['num'];
	$date1 = mktime(0,0,0,$m,$d,$y);
	$date2 = $date1 + 60 * 60 * 24 * $n;
	$fmt = "Y年n月j日(D)";
	$dstr = date($fmt,$date1);
	$res = date($fmt,$date2);
	echo $dstr . 'の'. $n . '日後は、' . $res;
?>

	</body>
</html>
