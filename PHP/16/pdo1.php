<?php
try {
	// データベースに接続する 
	$pdo = new PDO( 
		"mysql:host=localhost;dbname=sample;charset=utf8", // DSN 
		"root", // ユーザ名
		"himitu" // パスワード 
	 );
	// 例外処理の設定 
	$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); 
	echo "データベースに接続しました ";
} catch(PDOException $e) {
	echo $e->getMessage();
	exit;   
}
?>