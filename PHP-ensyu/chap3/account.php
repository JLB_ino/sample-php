<?php
	class Account {
		private $balance; //残高

		//コンストラクタ
		public function __construct($bal){
			$this->balance = $bal;
		}

		public function __set($var, $value){
			$this->{$var} = $value;
		}

		public function __get($var){
			return $this->{$var};
		}

		//入金用
		function deposit($amount){
			$this->balance += $amount;
		}

		//出金用
		function withdraw($amount){
			$this->balance -= $amount;
		}

	}
?>