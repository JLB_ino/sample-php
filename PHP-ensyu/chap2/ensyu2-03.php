<?php
	$lang = 'E';	// 	英語か日本語か指定
	$hour = 10;	// 時間を指定

	$func = 'ans_' . $lang;
	echo $hour . ': ' . $func($hour);

	function ans_J($n){
		$ans = array('おやすみ・・','おはよう','こんにちは','こんばんは');
		$num = getH($n);
		return $ans[$num];
	}

	function ans_E($n){
		$ans = array('Good night...','Good morning','Hello!','Good evening');
		$num = getH($n);
		return $ans[$num];
	}

	function getH($n){
		$hour = $n;
		if ($hour < 0) {$hour = 0;}
		if ($hour > 23) {$hour = 23;}
		return (int)($hour / 6);
	}
?>