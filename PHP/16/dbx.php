<?php 
// dbx.php DBXクラス⇒ データベース接続管理クラス
class DBX {    
	const DBHOST = "localhost";	//DBホスト名 
	const DBPORT = 3306;    	// DBポート番号
	const DBNAME = "sample"; 	// データベース名
	const DBCHAR = "utf8";    	//クライアント文字エンコード
	const DBUSER = "root";    	// DBユーザー名
	const DBPASS = "himitu";    	//パスワード
	public static $pdo;		// PDOオブジェクト
	
	// connect() ⇒ データベースに接続する
	public static function connect() {
		 self::$pdo = new PDO(
			"mysql:host=". static::DBHOST
          			. ";port=". self::DBPORT 
          			. ";dbname=". self::DBNAME
           			. ";charset=". self::DBCHAR,
			self::DBUSER, // ユーザー名 
			self::DBPASS // パスワード 
		); 
		// エラーを例外で処理する 
		self::$pdo->setAttribute(
			PDO::ATTR_ERRMODE,
			PDO::ERRMODE_EXCEPTION 
		);
	}
}
?>