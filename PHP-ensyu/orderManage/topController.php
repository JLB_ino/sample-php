<?php
/*
 * コントローラーの各メソッドではユーザーの要求を満たすために
 * まずはモデルを利用して、データを集め、集めたデータをビューに
 * 渡して表示させる。つまり、コントローラーはモデルとビューの
 * 橋渡しの役目を担う。
 */
require_once 'encode.php';
require_once 'view.php';

// リクエスト内容により処理の振り分けを行う
if (isset ( $_REQUEST ['request'] )) {
	$_func = e ( $_REQUEST ['request'] );
	$_func ();
} else {
	showIndex ();
}
function showIndex() {
	$view = new View ();
	$view->render ( "top_index.php" );
}
?>
